//#define XERR
#include "scanner.ih"

int Scanner::pushedStream()
{
    d_offset = d_input->lineOffset();

    if (d_pushed)
    {
        d_pushed = false;
        return '\n';
    }

    if (popStream())
        return '\n';

    toMs(StartCondition_::INITIAL);     // back to the spec file
    return 0;
}

