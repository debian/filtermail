//#define XERR
#include "expire.ih"

bool Expire::check()
{
    if (d_expiryDate.empty())
        return false;

    ifstream specs{ d_options.rulesStream() };

    string value;

    while (getline(specs, value))   // all lines in the specification file
    {
        value = String::trim(value);
        if (value.empty() or value.front() == '#')  // comment / empty lines:
            continue;                               // skipped

        processRules(value);
    }
    return true;
}
