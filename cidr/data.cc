#include "cidr.ih"

Pattern Cidr::s_pattern{ 
          R"(((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}))(/(\d{1,2}))?)" 
    };  //   12          3          4          5         6 7

    // 1: cidr address
    // 2..5: octets
    // 6: optional mask
    // 7: mask # bits to use



//    // plain IP address
//FBB::Pattern Cidr::s_ip4{ 
//                        R"(\[((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}))\])" 
//                    };
//    // match indices:
//    //      1:    full IP address
//    //      2..5: individual octets
