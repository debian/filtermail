//#define XERR
#include "inspectcidr.ih"

InspectCidr::InspectCidr(string const &range)
{
    if (range.find('/') != string::npos)    // IPv6 whois info may contain
    {                                       // its inspectcidr-range
        d_from = range;
        d_isInspectCidr = true;
        return;
    }

    d_isInspectCidr = false;
    istringstream in{ range };

    in >> d_from;
    d_first = binary(d_from);

    string address;
    in >> address >> address;       // skips the - between the addresses
    d_last = binary(address);
}
