//#define XERR
#include "rules.ih"

void Rules::update(ostream &file, size_t idx) const
{
    d_ruleVect[idx].writeNr(file);
    d_ruleVect[idx].writeDate(file);
}
