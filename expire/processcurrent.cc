//#define XERR
#include "expire.ih"

// char const *types[] = { "KEEP", "EXPIRE", "DONE" };

void Expire::processCurrent()
{
    Type type = KEEP;                               // initial entry type
    size_t begin = 0;                               // starts at offset 0
    size_t end = 0;

    cout << *d_fnamePtr << '\n';

    while (true)
    {
        Next next = entry();                       // get the next entry

        if (next.type == DONE)
        {
            if (not d_comment)
                write(type, begin, next.end);
            else
            {
                if (type == EXPIRE)
                {
                    write(type, begin, end);        // the expired entries
                    begin = end;
                }
                write(KEEP, begin, next.end);       // and the final comment
            }
            break;
        }            

        if (next.type == type)                      // process series of same
        {
            end = next.end;
            continue;                               // entry types as a block
        }

        write(type, begin, end);                    // write the 'type' block

        type = next.type;                           // and switch block-type
        begin = end;
        end = next.end;
    }
}







