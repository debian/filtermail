#define XERR
#include "stringcase.ih"

// overrides
bool StringCase::vMatch(string const &hdr) const
{
    return hdr.find(pattern()) != string::npos;
}
