//#define XERR
#include "cidr.ih"

// overrides
bool Cidr::vMatch(std::string const &headerLine) const
{
    string hdr{ headerLine };           // inspect this hdr line
    
    while (d_ip4 << hdr)                // find all matches
    {
        if (matchIP4())                 // this one matches: done.
            return true;
                                        // no match, test beyond the 
        hdr = d_ip4.beyond();           // last match
    }

    return false;        
}
