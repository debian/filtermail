//#define XERR
#include "inspector.ih"

void Inspector::skipThruReceived()
{
    string const &received = Options::instance().received();

    if (received.empty())                   // no --received: keep all hdrs
        return;

    auto iter = find_if(d_headers.begin(), d_headers.end(),     
        [&](string const &hdr)
        {                                   // stop when 'received' is found
            return hdr.find(received) != string::npos;
        }
    );

    if (iter == d_headers.end())
    {
        cout << "[warning] `" << received << 
                "' not found in the Received: headers\n\n";
        return;
    }

    d_headers.erase(d_headers.begin(), ++iter);
    if (d_headers.empty())
    {
        cout << 
            "[warning] no Received: header after the header containing `" << 
            received << "'\n";

        throw 0;                    // no need to continue the inspection
    }
}
