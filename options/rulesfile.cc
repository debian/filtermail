//#define XERR
#include "options.ih"

void Options::rulesFile()
{
    string fname;

    if (d_arg.option(&fname, 'r') == 0)
        throw Exception{} << "no rules file (--rules) specified";

    setBase(fname);

    d_rules.reset(new ifstream{ Exception::factory<ifstream>(fname) });
}
