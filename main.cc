#define XERR

#include "main.ih"

namespace
{
    ArgConfig::LongOption longOpts[] =
    {
        ArgConfig::LongOption{ "accept",        ArgConfig::Required },
        ArgConfig::LongOption{ "cls",           ArgConfig::Required },
        ArgConfig::LongOption{ "config",        'c' },
        ArgConfig::LongOption{ "expire",        'e' },
        ArgConfig::LongOption{ "help",          'h' },
        ArgConfig::LongOption{ "ignore",        ArgConfig::Required },
        ArgConfig::LongOption{ "interactive",   'i' },
        ArgConfig::LongOption{ "IP4-pattern",   'p' },
        ArgConfig::LongOption{ "log",           'l' },
        ArgConfig::LongOption{ "preamble",      ArgConfig::NoArg },
        ArgConfig::LongOption{ "received",      'R'},
        ArgConfig::LongOption{ "rules",         'r' },
        ArgConfig::LongOption{ "spam",          ArgConfig::Required },
        ArgConfig::LongOption{ "syntax",        ArgConfig::NoArg },
        ArgConfig::LongOption{ "version",       'v' },
    };
}

int main(int argc, char **argv)
try
{
    Options::initialize(
        usage, Icmbuild::version,
        ArgConfig::initialize(
            "c:e:hiIl:p:r:R:v", longOpts, end(longOpts), argc, argv
        )
    );

    if (ArgConfig::instance().option('I'))
    {
        Inspector{}.run();
        return 0;
    }

    preamble();                     // static data initializations

    if (Expire{}.check())
        return 0;

    Filter filter;                  // otherwise: filter the incoming mail

    inspect(filter);                // inspect the mail, set the filter action
    
    filter.done();                  // accept if no action so far
}
catch (...)
{
    return handleException();
}


