//#define XERR
#include "inspector.ih"

Pattern Inspector::s_cidr{ 
              R"(((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}))(/(\d{1,2}))?)" 
        };  //   12          3          4          5         6 7

Pattern Inspector::s_cidr6{ 
              R"(\[(([[:xdigit:]]{1,4})(:[[:xdigit:]]{0,4}){1,7})\])"
        };  //     12
