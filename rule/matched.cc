//#define XERR
#include "rule.ih"

bool Rule::matched()
{
    if (d_matched)
        update();

    return d_matched;               // return whether this Rule is matched
}
        
