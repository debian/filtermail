//#define XERR
#include "log.ih"

Log &operator<<(Log &log, std::ostream &(*func)(std::ostream &))
{
    if (log.d_outPtr)
        (*func)(*log.d_outPtr);
    return log;
}
