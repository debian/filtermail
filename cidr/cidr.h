#ifndef INCLUDED_CIDR_
#define INCLUDED_CIDR_

#include <bobcat/pattern>

#include "../specbase/specbase.h"

class Cidr: public SpecBase
{
    uint32_t d_cidr = 0;                    // binary value of the pattern
    uint32_t d_mask = 0;                    // #bits to use

    mutable FBB::Pattern d_ip4;             // plain IP4 dotted decimal addr.

    static FBB::Pattern s_pattern;          // to match 'pattern'

    public:
        Cidr(std::string const &ip4);                                   // .f
        ~Cidr() override;

    private:
        bool vMatch(std::string const &hdr) const override;
        void vPattern(std::string const &pattern) override;

                                            // true: s_ip4 matches
        bool matchIP4() const;              // d_cidr & d_mask          .f

        static uint32_t binary(FBB::Pattern const &pattern);
        static uint32_t mask();             // uses s_pattern
};

#include "cidr.f"

#endif
