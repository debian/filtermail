//#define XERR
#include "inspectcidr.ih"

inline std::ostream &operator<<(std::ostream &out, InspectCidr const &inspectcidr)
{
    return inspectcidr.insert(out);
}
