//#define XERR
#include "inspector.ih"

    // cf. https://en.wikipedia.org/wiki/Private_network

bool Inspector::ip172_16() const
{
        // the 2nd term checks whether the content of the 2nd octet = 0x1.
        // and thus whether d_ip matches 172.16.0.0/16

    return s_cidr[2] == "172" and (stoul(s_cidr[3]) >> 4) == 1;
}
