#include "parser.ih"

void Parser::error()
{
    // if d_skip is set, then don't show the message and reduce d_nErrors_

    Log &log = Log::instance();

    log << "Syntax error in " << d_scanner.filename() << ", line " <<
            d_scanner.lineNr() << " expected " << d_expect;

    if (d_received)
        log << ", received `" << d_scanner.matched() << '\'';

    log << endl;

    d_received = true;
}

