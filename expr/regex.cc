//#define XERR
#include "expr.ih"

bool Expr::regex(string const &txt)
{
    if (d_available)                // Expr was already loaded
        return d_available;
    
    d_regex = txt;

    d_regex.pop_back();                 // remove the final quote

    size_t pos = 0;                     // change all \' into '
    while ((pos = d_regex.find("\\'", pos)) != string::npos)
        d_regex.erase(pos, 1);
                                        // rexexes must have content, other
                                        // than spaces, tabs and quotes
    return d_available = d_regex.find_first_not_of("' \t") != string::npos;
}
