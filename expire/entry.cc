//#define XERR
#include "expire.ih"

Expire::Next Expire::entry()
{
    string line;

    d_comment = false;

    while (true)
    {
        getline(d_current, line);
        if (not d_current)
            break;

        line = String::trim(line);
        if (line.empty())
            continue;

        if (line.front() == '#')    // comment line
        {
            if (line == "#=")           // 1st #= defines an entry
            {
                if (not d_first)        // not the 1st -> plain commnt
                    continue;
    
                d_first = false;
                return { KEEP, d_current.tellg() };
            }
            d_comment = true;
            continue;               // normal comment: read the next line
        }

        d_first = false;            // real entry: no more first #= checks
        d_comment = false;
        return { expired(line) ? EXPIRE : KEEP, d_current.tellg() };
    }

    d_current.clear();
    return { DONE, d_current.seekg(0, ios::end).tellg() };    
}

