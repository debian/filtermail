//#define XERR
#include "rules.ih"

void Rules::reset()
{
    for (Rule &rule: d_ruleVect)
        rule.reset();

    d_matched.clear();
    d_next = 0;
}
