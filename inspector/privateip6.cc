//#define XERR
#include "inspector.ih"

    // cf. https://en.wikipedia.org/wiki/Private_network:
    //     The address block fc00::/7 is reserved by IANA for unique local
    //     addresses

bool Inspector::privateIP6() const
{
    return ((stoul(s_cidr6[2], 0, 16) >> 8) & ~1UL) == 0xfc;
}
