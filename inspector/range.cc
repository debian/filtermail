//#define XERR
#include "inspector.ih"

// static
bool Inspector::range(string const &line, StrVect const &keywords)
{
    for (string const &keyword: keywords)
    {
        if (line.find(keyword) == 0)
        return true;
    }

    return false;
}
