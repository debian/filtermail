//#define XERR
#include "stringnocase.ih"

// overrides
bool StringNoCase::vMatch(string const &hdr) const
{
    size_t length = pattern().length();
    int lastIdx = hdr.length() - length;

    if (lastIdx < 0)
        return false;

    char const *cHdr = &hdr.front();
    char const *cPattern = &pattern().front();

    size_t hdrIdx = 0;
    while (true)
    {
        hdrIdx = hdr.find_first_of(d_first, hdrIdx);
        if (
            hdrIdx == string::npos      // first pattern char. not found
            or 
            static_cast<int>(hdrIdx) > lastIdx // pattern longer than the rest
        )
            return false;               // then no match

                                        // case insensitive match 
        if (strncasecmp(cPattern, cHdr + hdrIdx, length) == 0)
            return true;
        
        ++hdrIdx;                       // continue at the next char.
    }
}



