//#define XERR
#include "condition.ih"

bool Condition::matched(bool rulesMatched)
{
    return d_matched ? 
                (d_matched = rulesMatched)
            : 
                false;
}
