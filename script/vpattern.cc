//#define XERR
#include "script.ih"

// overrides
void Script::vPattern(std::string const &script) 
{
    istringstream in{ script };

    string first;
    in >> first;            // there is content, so this succeeds

    d_mode =    first.find("SHELL") == 0 ? Proc::USE_SHELL : Proc::NO_PATH;

    if (d_mode == Proc::NO_PATH)
        d_command = script;
    else
        getline(in, d_command);

    d_command = String::trim(d_command);
    Options::setBase(d_command);
}
