#define XERR
#include "inspector.ih"

void Inspector::run()
{
    bool hit = false;

    for (string const &hdr: d_headers)
    {
        if (not findIP(hdr))            // find an IP address in hdr
            continue;

        hit = true;
        findWhoIs();                    // find the whois net-ranges
        findCidr();                     // find the cidr-ranges of d_ranges
    }

    if (not hit)
        cout << "No public IP addresses found in the Received: headers\n";
}
