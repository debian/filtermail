#ifndef INCLUDED_CONDITION_
#define INCLUDED_CONDITION_

#include <iosfwd>

class Rules;

class Condition
{
    bool d_matched;
    Rules *d_rules;

    public:
        Condition();

        bool matched() const;                                           // .f
        bool matched(bool rulesMatched);

        void reset();               // d_matched = true, next Cond term    .f
        Rules &rules(Rules &rules);                                     // .f
};

#include "condition.f"
        
#endif
