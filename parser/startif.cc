//#define XERR
#include "parser.ih"

void Parser::startIf()
{
    d_onlyParse = d_syntax or d_nErrors_ > 0;
    d_filter.startIf(d_scanner.lineNr());
    d_scanner.allowFileSwitching();
    d_expect = "mailHeader[*]";
}
