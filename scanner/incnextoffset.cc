//#define XERR
#include "scanner.ih"

void Scanner::incNextOffset()    // called in lexer when #=\n is encountered
{
    if (d_nextOffset == 0)
    {
        d_nextOffset = d_input->lineOffset();
        d_offset = d_nextOffset;
    }
}
