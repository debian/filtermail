//#define XERR
#include "current.ih"

namespace
{
    char const *s_action[] =
    {
        "ACCEPT",
        "IGNORE",
        "SPAM",
    };
}

void Current::log(eAction type)
{
    Log &log = Log::instance();

    if (d_rulesLine == string::npos)
        return;

    log << "Rules line " << d_rulesLine << " (" << s_action[type] << "): ";

    for (Data const &data: d_data)
    {
        log << data.filename << " header `" << data.header << "' ";

        if (data.idx == string::npos)
            log << "no match; ";
        else
            log << "matched by rule " << data.idx << "; ";
    }
    
    log << endl;

    d_rulesLine = string::npos;
}
