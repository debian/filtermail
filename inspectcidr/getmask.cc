//#define XERR
#include "inspectcidr.ih"

size_t InspectCidr::getMask() const
{
    size_t cmp = d_first ^ d_last;         // find the unequal bits

    size_t mask = 0;                    // mask: the # equal bits, start at 31
    for (size_t bit = 32; bit--; )
    {
        if ((cmp & (1 << 31)) != 0)     // stop once the most sign. bit != 0
            break;

        cmp <<= 1;                      // the MSB - 1 becomes the MSB
        ++mask;                         // inc. the # identical MS bits
    }
    
    return mask;
}
