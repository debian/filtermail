//#define XERR
#include "rules.ih"

void Rules::details(string const &fname, size_t idx)
{
                                                // the rules file to update
    fstream file = Exception::factory<fstream>(fname);

    if (idx == 0)                               // update the toprule
        update(file, 0);
    else
        swapRule(file, idx);                    // swap with the previous rule
}
