inline void Filter::addRule(size_t begin, size_t rule, 
                            std::string const &nrTxt, size_t beyondIdx)
{
    d_rule = &d_rules->addRule(begin, rule, nrTxt, beyondIdx);
}

inline void Filter::matchMode(eMatchMode mode) 
{
    d_expr = &d_rule->matchMode(mode);
}

inline void Filter::endRule(size_t offset)
{
    d_rule->setEnd(offset);
}

inline bool Filter::matchCondition()
{
    return d_condition.matched(d_rules->ruleMatched());
}

inline void Filter::matchExpr(eTruth truth)
{
    d_rule->matchExpr(truth);
}

inline bool Filter::regex(std::string const &txt)
{
    return d_expr->regex(txt);
}

inline void Filter::setDate(std::string const &date, size_t idx)
{
    d_rule->setDate(date, idx);
}
