//#define XERR
#include "filter.ih"

void Filter::startIf(size_t lineNr)
{
    d_matchedFiles.clear();     // no files having matched rules yet
    d_condition.reset();        // assume the condition(s) match

    d_current.reset(lineNr);
    d_rules = 0;
}
