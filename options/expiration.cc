//#define XERR
#include "options.ih"

bool Options::expiration()
{
    if (d_expire.empty())
        return false;

    if (d_interactive)
        throw Exception{} << "options --expire and --interactive cannot both "
                             "be specified";

    if (not (Pattern{ R"(\d{2}-\d{2}-\d{2})" } << d_expire))
        throw Exception{} << 
                "Expiration date must be yy-mm-dd. Received: `" <<
                d_expire << '\''; 

    return not d_preamble;
}
