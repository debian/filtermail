#ifndef INCLUDED_RULEPATTERN_
#define INCLUDED_RULEPATTERN_

#include <bobcat/pattern>

#include "../specbase/specbase.h"

class RulePattern: public SpecBase
{
    bool d_useCase;
    mutable FBB::Pattern d_pattern;

    public:
        RulePattern(bool useCase);
        ~RulePattern() override;

    private:
        bool vMatch(std::string const &hdr) const override;
        void vPattern(std::string const &pattern) override;
};

#endif
