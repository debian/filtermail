//#define XERR
#include "parser.ih"

void Parser::regex()
{
    if (not d_onlyParse)
        d_onlyParse = not d_filter.regex(matched());
}
