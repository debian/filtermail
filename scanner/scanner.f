inline eAction Scanner::action() const
{
    return d_action;
}

inline void Scanner::stopFileSwitching()
{
    d_pushStreamOK = false;
}

// $insert inlineLexFunction
inline int Scanner::lex()
{
    return lex_();
}

inline void Scanner::preCode() 
{
    // optionally replace by your own code
}

inline void Scanner::postCode([[maybe_unused]] PostEnum_ type) 
{
    // optionally replace by your own code
}

inline void Scanner::print() 
{
    print_();
}

inline size_t Scanner::offset() const
{
    return d_offset;
}

inline size_t Scanner::nextOffset() const
{
    return d_nextOffset;
}

inline void Scanner::setNextOffset()
{
    d_nextOffset = d_offset;
}

inline size_t Scanner::idx() const
{
    return d_idx;
}
