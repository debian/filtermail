#include "../VERSION"

namespace Icmbuild
{
    char const version[]    = VERSION;
    char const years[]      = YEARS;
    char const author[]     = AUTHOR;
}

