//#define XERR
#include "options.ih"

// static
void Options::initialize(void (*usage)(string const &),
                         char const *version, ArgConfig &arg)
{
    if (s_optionsPtr)
        throw Exception{} << "Options object already initialized";

    s_optionsPtr.reset( new Options{ usage, version, arg } );
}
