//#define XERR
#include "rule.ih"

void Rule::writeDate(ostream &out) const
{
    out.seekp(d_ruleOffset + d_dateIdx);
    out << d_date;
}
