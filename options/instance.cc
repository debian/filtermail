//#define XERR
#include "options.ih"

// static
Options &Options::instance()
{
    if (not s_optionsPtr)
        throw Exception{} << "Options object not initialized";

    return *s_optionsPtr;
}

