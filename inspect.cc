// #define XERR
#include "main.ih"

void inspect(Filter &filter)
try
{
    Parser parser{ filter, Options::instance().syntax() };
                                // the parser forwards its findings to
    parser.parse();             // filter, Expr performs the filtering
}
catch (exception const &exc)
{
    Log::instance() << exc.what() << ": by default accepting e-mail" << endl;
}

