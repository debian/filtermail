//#define XERR
#include "rule.ih"

// static
void Rule::setNow()
{
    time_t secs = time(0);
    ostringstream out;
    out << put_time(localtime(&secs), "%y-%m-%d");
    s_now = out.str();
}
