#define XERR
#include "filter.ih"

void Filter::done()
{
    Log &log = Log::instance();
    Options &options = Options::instance();

    if (options.syntax())
    {
        log << "syntax check completed" << endl;
        return;
    }

    if (not d_condition.matched())
        log << "mail accepted by default" << endl;

                                                // use the proper mail file
                                                // handler
    Mail &mail = Mail::instance();

    ostream *out = options.mailFile(d_action);
    if (not out)                                // mail is ignored
        return;

    if (not options.logIgnored(d_action))       // mail is appended
        *out << mail << '\n' << Expr::input().rdbuf();
    else            
    {                                           // only headers are appended
        mail.hdr("From:");
        string from = mail.headers().front();
        mail.hdr("Subject:");
        *out  << "Received " << Options::mailType(d_action) << " mail:\n"
                "   From: " << from << "\n"
                "   Subject: " << mail.headers().front() << endl;
    }
}


