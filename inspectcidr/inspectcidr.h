#ifndef INCLUDED_INSPECTCIDR_
#define INCLUDED_INSPECTCIDR_

#include <ostream>
#include <string>

class InspectCidr
{
    friend std::ostream &operator<<(std::ostream &out, 
                                    InspectCidr const &inspectcidr);

    bool d_isInspectCidr;           // d_from is already a inspectcidr range
    std::string d_from;             // the from INSPECTCIDR address 
    
    size_t d_first;
    size_t d_last;

    public:
                                    // the netrange found by whois
        InspectCidr(std::string const &range);

    private:
        static size_t binary(std::string const &address);
        size_t getMask() const;
        std::ostream &insert(std::ostream &out) const;
};
        
#include "inspectcidr.f"

#endif
