//#define XERR
#include "rule.ih"

Rule::Rule(size_t begin, size_t rule, string const &nrTxt, size_t beyondIdx)
:
    d_available(false),
    d_matched(true),

    d_nr(nrTxt),
    d_beyondIdx(beyondIdx),

    d_beginOffset(begin),
    d_ruleOffset(rule)
{
    reset();
}
