#include "parser.ih"

unordered_map<int, eMatchMode> Parser::s_matchMode 
{
    { 'c', CIDR             },
    { 's', STRING           },
    { 'i', STRING_NOCASE    },
    { 'p', PATTERN          },
    { 'n', PATTERN_NOCASE   },
    { '$', SCRIPT           },
};
