#define XERR
#include "expire.ih"

Expire::Expire()
:
    d_options(Options::instance())
{
    d_expiryDate = d_options.expire();
    if (d_expiryDate.empty())
        return;

    Log &log = Log::instance();

    auto [begin, end] = d_options.beginEndRE("^\\s*dont-expire:\\s");
    if (begin != end)
        log << "Not inspecting expiration dates of:" << endl;

                                    // add ignored files in the config file
    for (string const &line: ranger(begin, end))
    {
        istringstream in{ line };
        string value;
        in >> value >> value;           // 2nd entry is the filename

        log << "    " << value << endl;
        d_skip.insert(value);
    }
}






