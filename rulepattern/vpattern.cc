//#define XERR
#include "rulepattern.ih"

// overrides
void RulePattern::vPattern(string const &pattern)
{
    d_pattern = FBB::Pattern(pattern, d_useCase);
}
