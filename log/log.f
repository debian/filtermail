template <typename Type>
Log &operator<<(Log &log, Type const &type)
{
    if (log.d_outPtr)
        *log.d_outPtr << type;

    return log;
}

