//#define XERR
#include "inspectcidr.ih"

ostream &InspectCidr::insert(ostream &out) const
{
    out << d_from;
    if (not d_isInspectCidr)
        out << '/' << getMask();
    return out;
}
