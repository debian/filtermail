#define XERR
#include "filter.ih"

bool Filter::matchRule()
{
    bool ret = d_rules->matchRule();
    if (ret)
    {
        d_current.matchedRule(d_rules->idx());
        d_matchedFiles.insert(d_filename);
    }

    return ret;
}

        
