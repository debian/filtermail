#include "parser.ih"

Parser::Parser(Filter &filter, bool syntax)
:
    d_filter(filter),
    d_expect("if"),
    d_received(true),
    d_syntax(syntax)
{
    if (d_syntax)
        Log::instance() << "syntax check" << endl;
}

