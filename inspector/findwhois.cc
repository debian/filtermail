#define XERR
#include "inspector.ih"

void Inspector::findWhoIs()
{
    d_io.clear();
    d_io.str("");

    d_proc.setCommand( "whois " + d_ip);
    d_proc | d_io;                        // collect the whois output

    d_ranges.clear();                       // store the whois-ranges 
    d_io.seekg(0);
    string line;
    while (getline(d_io, line))
    {
        if (range(line, { "NetRange:", "inetnum:", "inet6num:" }))
            d_ranges.push_back(line.substr(line.find(':') + 1));
        else if (line.find("ountry:") == 1)
        {
            cout << "Country:" << line.substr(line.rfind(' ')) << ",  ";
            return;
        }
    }
}

