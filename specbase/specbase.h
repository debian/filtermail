#ifndef INCLUDED_SPECBASE_
#define INCLUDED_SPECBASE_

#include <string>

struct SpecBase
{
    virtual ~SpecBase();

    void pattern(std::string const &txt);                           // .f
    bool match(std::string const &hdr) const;                       // .f

    private:
        virtual bool vMatch(std::string const &hdr) const = 0;
        virtual void vPattern(std::string const &pattern) = 0;
};

#include "specbase.f"

#endif
