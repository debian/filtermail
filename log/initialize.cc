//#define XERR
#include "log.ih"

// static
void Log::initialize(ostream *outPtr)
{
    if (s_logPtr)
        throw Exception{} << "Log object already initialized";

    s_logPtr.reset( new Log{ outPtr } );
}
