#define XERR
#include "mail.ih"

Mail::Mail()
:
    d_mh(cin,                                   // get the mail headers
        Options::instance().syntax() ?          // unless doing syntax checks
            MailHeaders::DONT_READ : MailHeaders::READ)
{}
