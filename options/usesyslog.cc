//#define XERR
#include "options.ih"

namespace
{
    unordered_map<string, Facility> n_facility      // n_: namespace spec.
    {
        { "LOCAL0", LOCAL0 },
        { "LOCAL1", LOCAL1 },
        { "LOCAL2", LOCAL2 },
        { "LOCAL3", LOCAL3 },
        { "LOCAL4", LOCAL4 },
        { "LOCAL5", LOCAL5 },
        { "LOCAL6", LOCAL6 },
        { "LOCAL7", LOCAL7 },
        { "MAIL",   MAIL },
        { "USER",   USER },
    };

    unordered_map<string, Priority> n_priority
    {
        { "EMERG",      EMERG },
        { "ALERT",      ALERT },
        { "CRIT",       CRIT },
        { "ERR",        ERR },
        { "WARNING",    WARNING },
        { "NOTICE",     NOTICE },
        { "INFO",       INFO },
        { "DEBUG",      DEBUG },
    };
}

void Options::useSyslog(string const &spec)
{
    size_t pos = spec.find(':');

    auto facility = n_facility.find(spec.substr(0, pos));
    auto priority = n_priority.find(spec.substr(pos + 1));

    if (priority == n_priority.end() or facility == n_facility.end())
        throw Exception{} << spec << ": invalid facility or level";

    d_syslogBufPtr.reset(new SyslogBuf{ 
                                ArgConfig::instance().basename(),
                                priority->second, facility->second
                                    }
                        );

    d_logPtr.reset(new ostream{ d_syslogBufPtr.get() });
}
