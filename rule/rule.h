#ifndef INCLUDED_RULE_
#define INCLUDED_RULE_

#include <iostream>
#include <string>
#include <vector>

#include "../expr/expr.h"

using ExprVect = std::vector<Expr>;

class Rule
{
    static std::string s_now;

    bool d_available;
    bool d_matched;

    std::string d_date;     // the date spec.
    size_t  d_dateIdx;      // the date stamp idx in the Rule line 

    std::string d_nr;       // the text representing the hit-count
    size_t  d_beyondIdx;    // the line idx beyond d_nr

    size_t  d_beginOffset;  // the offset where the rule + comment starts
    size_t  d_ruleOffset;   // offset of the line defining the Rule
    size_t  d_endOffset;    // offset of the next line
        
    ExprVect d_exprVect;    // the 'Expr's of this Rule
    size_t d_next;          // the idx of the next Expr object to process
    Expr *d_expr;           // ptr to the currently processed  Expr

    public:
        Rule(size_t begin, size_t rule, std::string const &nrTxt, 
             size_t beyondIdx); 

        size_t beginOffset() const;                                 //  .f

        Expr &matchMode(eMatchMode mode);   // Expr receives the eMatchMode

        std::string const &date() const;                            // .f
        size_t endOffset() const;                                   // .f

                                        // inline?
        bool matched();      // final eTruth update of the Rule

        void matchExpr(eTruth truth);    // evaluate the current expression

        void reset();                   // initialize to d_matched: true

                                        // set the rule's Date info
        void setDate(std::string const &date, size_t beyondIdx);       

        void setEnd(size_t offset);                                 // .f

        size_t size() const;            // # Expr objects           // .f

        void update();                  // update nr and date

        size_t width() const;           // # bytes of this Rule     // .f

        void writeDate(std::ostream &out) const;
        void writeNr(std::ostream &out) const;

        static void setNow();
};

#include "rule.f"

#endif
