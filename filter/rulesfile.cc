#define XERR
#include "filter.ih"

void Filter::rulesFile(string const &filename)
{
    d_filename = filename;

    d_current.ruleFile(d_filename);

    auto next = d_map.insert({ filename, Rules{} });

    d_rules =                   // make the file's Rules directly available
        &d_condition.rules(next.first->second);

    d_rules->reset();
}
