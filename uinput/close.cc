#include "uinput.ih"

void UInput::close()    // force closing the stream
{
    delete d_in;
    d_in = 0;
}

