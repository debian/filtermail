#define XERR
#include "expire.ih"

void Expire::processRules(string const &ifspec)
{
    istringstream in{ ifspec };     // extract elements fm the specification

    string entry;
    while (in >> entry)
    {
        if (
            entry.front() == '.'                            // a file name
            and d_skip.find(entry) == d_skip.end()    // not yet seen
        )
        {
            d_skip.insert(entry);
            ruleFile(entry);
        }
    }
}
