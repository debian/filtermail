#include "scanner.ih"

Scanner::Scanner()
:
    ifstream(Options::instance().rulesStream()),
    ScannerBase(*this, cout),
    d_pushStreamOK(true),
    d_pushed(false)
{}

