#define XERR
#include "cidr.ih"

//  pattern like 209.85.218.45
// cf. data.cc:
// s_ip4{R"(((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})))" 
//          12          3          4          5
//  elements 2..5 contain the subsequent elements of the IP address

// static
uint32_t Cidr::binary(Pattern const &pattern)
{
    uint32_t ret = 0;

    for (size_t value, idx = 2; idx != 6; ++idx)
    {
        ret <<= 8;
        value = stoul(pattern[idx]);
        ret |= value;
    }

    return ret;
}
