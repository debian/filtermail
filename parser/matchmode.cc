#include "parser.ih"

void Parser::matchMode()
{
    d_expect = "regex";

    auto iter = s_matchMode.find(matched().front());

    if (iter == s_matchMode.end())          // unknown match mode
    {
        cout << "Unknown match mode `" << matched() << "'\n";
        d_onlyParse = true;
    }

    if (not d_onlyParse)
        d_filter.matchMode(iter->second);
}
