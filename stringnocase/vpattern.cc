//#define XERR
#include "stringnocase.ih"

void StringNoCase::vPattern(string const &pattern)
{
    setPattern(pattern);

    char ch = pattern.front();
    d_first = ch;
    if (isalpha(ch))
        d_first += islower(ch) ? toupper(ch) : tolower(ch);

}
