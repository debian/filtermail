//#define XERR
#include "inspector.ih"

void Inspector::receivedHeaders()
{
    MailHeaders mh(cin);

    mh.setHeaderIterator("Received");

    for (auto begin = mh.beginh(),  end = mh.endh(); begin != end; ++begin)
       d_headers.push_back(*begin);

    skipThruReceived();
}
