//#define XERR
#include "expr.ih"

bool Expr::s_interactive = false;
size_t Expr::s_id = 0;

vector<Expr::UptrSpecBase> Expr::s_matchPtr(eMatchModeSize);

unique_ptr<OneKey> Expr::s_oneKey;

unique_ptr<TempStream> Expr::s_headers;
unique_ptr<TempStream> Expr::s_input;
