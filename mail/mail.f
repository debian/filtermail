inline Mail::VectStr const &Mail::headers() const
{
    return d_headers[d_hdrsIdx];
}

inline std::ostream &operator<<(std::ostream &out, Mail const &mail)
{
    return mail.insert(out);
}
