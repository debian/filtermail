inline bool Condition::matched() const
{
    return d_matched;
}

inline void Condition::reset()
{
    d_matched = true;
}

inline Rules &Condition::rules(Rules &rules)
{
    return *(d_rules = &rules);
}
        
