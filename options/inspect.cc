//#define XERR
#include "options.ih"

bool Options::inspect()
{
    if (not d_inspect)
        return false;

    d_arg.option(&d_received, 'R');

    string what;
    if (size_t count = d_arg.option(&what, "cls"); count <= 1)
        d_cls = count == 0 || what == "yes";
    else
        throw Exception{} << "Option --cls can only be specified once";

    xerr("reeived: " << d_received);

    return true;
}
