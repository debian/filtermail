//#define XERR
#include "mail.ih"

//static
string Mail::rmHeader(string const &header)
{
    string ret;

    size_t pos = header.find_first_of(" \t");   // space after the header

    if (pos == string::npos)                    // no space? empty content
        return ret;

    pos = header.find_first_not_of(" \t", pos); // content following " \t"

    if (pos != string::npos)                    // found content
        ret = header.substr(pos);

    return ret;
}
