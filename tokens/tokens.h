#ifndef INCLUDED_TOKENS_
#define INCLUDED_TOKENS_

struct Tokens
{
    // Symbolic tokens:
    enum Tokens_
    {
        IF = 257,
        ACTION,
        DATE,
        FILE,
        HDR,
        LETTER,
        NOT,
        NR,
        REGEX,
        AND,
    };

};

#endif
