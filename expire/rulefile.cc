//#define XERR
#include "expire.ih"

//    rule file processing:
//        collect all lines until #= or a rule-line
//        at #=: continue, write lines-so-far to updated, 
//                         handle future #= lines as comment
//        at a rule line:

#include <iostream>
    
void Expire::ruleFile(string const &fname)
{
    d_first = true;                                 // for 1st #= comment
        
                                                    //the current rules-file
    d_current = Exception::factory<ifstream>(fname);
    d_fnamePtr = &fname;                            // and its name

    processCurrent();

    if (d_expired)
    {
        d_current.close();
        d_updated->close();

//cout << "Enter 2: ";
//string line;
//getline(cin, line);
        rename(d_updated->fileName(), fname);
    }

    d_updated.reset(0);
    d_expired.reset(0);
}






