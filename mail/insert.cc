//#define XERR
#include "mail.ih"

ostream &Mail::insert(ostream &out) const
{
    copy(d_mh.begin(), d_mh.end(), ostream_iterator<string>(out, "\n"));
    return out;
}
