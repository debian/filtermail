#ifndef INCLUDED_INSPECTOR_
#define INCLUDED_INSPECTOR_

#include <sstream>
#include <string>
#include <vector>

#include <bobcat/pattern>
#include <bobcat/proc>

class Inspector
{
    enum IPType
    {
        NONE,
        IPv4,
        IPv6
    };

    using StrVect = std::vector<std::string>;

    FBB::Proc d_proc;                   // inspects PATH
    StrVect d_headers;
    StrVect d_ranges;                   // whois-IP ranges

    std::stringstream d_io;             // collects d_proc output
    std::string d_ip;                   // the IP address found in a hdr
    std::string d_received;             // skip hdrs thru one containing this

    IPType d_ipType;

    static FBB::Pattern s_cidr;         // generic IPv4cidr pattern
    static FBB::Pattern s_cidr6;        // generic IPv6 cidr pattern

    public:
        Inspector();
        void run();                     // process the Received: headers

    private:
        bool findIP(std::string const &hdr);    // true: found d_ip in hdr
        void findWhoIs();               // find the whois range
        void findCidr();                // find the cidr range
        bool ip172_16() const;          // true: IP4 address is 172.16.0.0/20
        bool privateIP4() const;        // true: private IPv4 address
        bool privateIP6() const;        // true: private IPv6 addres
        static bool range(std::string const &line, StrVect const &keywords);
        void receivedHeaders();         // determine the received headers
        void skipThruReceived();        // rm headers thru the --received hdr
};
        
#endif
