//#define XERR
#include "mail.ih"

//static
void Mail::initialize(bool interactive)
{
    if (interactive)
        return;

    s_mail.reset(new Mail);
}
