inline std::string const &SpecString::pattern() const
{
    return d_pattern;
}
        
inline void SpecString::setPattern(std::string const &pattern)
{
    d_pattern = pattern;
}
        
